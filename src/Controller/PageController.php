<?php

namespace App\Controller;

use App\Factory\ProductFactory;
use App\Form\ProductFormData;
use App\Form\ProductType;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{
    public function __construct(private readonly ProductFactory $productFactory)
    {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function findProduct(Request $request): Response
    {
        $productFormData = new ProductFormData();
        $form = $this->createForm(ProductType::class, $productFormData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ProductFormData $productFormData */
            $productFormData = $form->getData();
            $productFormData = $this->productFactory->findProduct($productFormData->getProductId());

            return $this->json([
                'product' => $productFormData->toArray(),
            ]);
        }

        return $this->render('page/findProduct.html.twig', [
            'form' => $form,
        ]);
    }
}