<?php

namespace App\Form;

class ProductFormData
{
    protected string $productId;

    public function getProductId(): string
    {
        return $this->productId;
    }

    /** @noinspection PhpUnused */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }
}