<?php

namespace App\Repository;

class ElasticSearchDriver implements IElasticSearchDriver
{

    public function findById(string $id): array
    {
        return [
            'id' => $id,
            'data' => [
                'driver' => 'ElasticSearchDriver',
            ],
        ];
    }
}