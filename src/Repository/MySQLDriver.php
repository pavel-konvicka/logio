<?php

namespace App\Repository;

class MySQLDriver implements IMySQLDriver
{

    public function findProduct(string $id): array
    {
        return [
            'id' => $id,
            'data' => [
                'driver' => 'MySQLDriver',
            ],
        ];
    }
}