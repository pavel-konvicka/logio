<?php

namespace App\ValueObject;

class ProductData
{
    private string $productId;

    private array $data;

    private int $requestCount;

    /**
     * @param array{
     *     id: string,
     *     data: array
     * } $data
     */
    public function __construct(array $data, int $requestCount)
    {
        $this->productId = $data['id'];
        $this->data = $data['data'];
        $this->requestCount = $requestCount;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'data' => $this->getData(),
            'requestCount' => $this->getRequestCount(),
        ];
    }

    public function getId(): string
    {
        return $this->productId;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getRequestCount(): int
    {
        return $this->requestCount;
    }
}