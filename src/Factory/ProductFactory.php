<?php

namespace App\Factory;

use App\Adapter\ILogioProductStatisticsCache;
use App\Repository\MySQLDriver;
use App\Repository\ElasticSearchDriver;
use App\ValueObject\ProductData;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;

class ProductFactory
{
    private const PRODUCT_CACHE_KEY = 'product_%s';
    private const PRODUCT_STATISTICS_CACHE_KEY = 'statistics_product_%s';

    public function __construct(
        private readonly MySQLDriver                  $mySQLDriver,
        private readonly CacheInterface               $productCacheDriver,
        private readonly ElasticSearchDriver          $elasticSearchDriver,
        private readonly bool                         $elasticSearchDriverEnable,
        private readonly ILogioProductStatisticsCache $productStatisticsCacheDriver,
    )
    {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function findProduct(string $id): ProductData
    {
        $requestCount = $this->increaseRequestCount($id);
        $product = $this->productCacheDriver->get(sprintf(self::PRODUCT_CACHE_KEY, $id), function () use ($id) {
            if ($this->elasticSearchDriverEnable) {
                $product = $this->elasticSearchDriver->findById($id);

                if ($product) {
                    return $product;
                }
            }

            return $this->mySQLDriver->findProduct($id);
        });

        return new ProductData($product, $requestCount);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function increaseRequestCount(string $id): int
    {
        $cacheKey = sprintf(self::PRODUCT_STATISTICS_CACHE_KEY, $id);
        $productStatistics = $this->productStatisticsCacheDriver->getItem($cacheKey);
        $count = $productStatistics->get() ?? 0;

        $count += 1;
        $productStatistics->set([$cacheKey => $count]);
        $this->productStatisticsCacheDriver->save($productStatistics);

        return $count;
    }
}