<?php

namespace App\Adapter;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Cache\CacheInterface;

interface ILogioProductStatisticsCache extends AdapterInterface, CacheInterface
{
    public function __construct(string $location);
}