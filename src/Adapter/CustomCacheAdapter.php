<?php

namespace App\Adapter;

use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\CacheItem;

class CustomCacheAdapter implements ILogioProductStatisticsCache
{
    public function __construct(private readonly string $location)
    {
        $this->checkDir();
    }

    public function getItem(mixed $key): CacheItem
    {
        $cacheItem = new CacheItem();

        if (!file_exists($this->location)) {
            return $cacheItem;
        }

        $content = json_decode(file_get_contents($this->location), true);

        foreach ($content as $product => $value) {
            if ($product === $key) {
                $cacheItem->set($value);
            }
        }

        return $cacheItem;
    }

    public function getItems(array $keys = []): iterable
    {
        // TODO: Implement getItems() method.
    }

    public function clear(string $prefix = ''): bool
    {
        // TODO: Implement clear() method.
    }

    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null): mixed
    {
        // TODO: Implement get() method.
    }

    public function delete(string $key): bool
    {
        // TODO: Implement delete() method.
    }

    public function hasItem(string $key): bool
    {
        // TODO: Implement hasItem() method.
    }

    public function deleteItem(string $key): bool
    {
        // TODO: Implement deleteItem() method.
    }

    public function deleteItems(array $keys): bool
    {
        // TODO: Implement deleteItems() method.
    }

    public function save(CacheItemInterface $item): bool
    {
        if (file_exists($this->location)) {
            $content = json_decode(file_get_contents($this->location), true);
        }

        $key = key($item->get());
        $content[$key] = $item->get()[$key];
        $status = file_put_contents($this->location, json_encode($content));

        if ($status === false) {
            return false;
        }

        return true;
    }

    public function saveDeferred(CacheItemInterface $item): bool
    {
        // TODO: Implement saveDeferred() method.
    }

    public function commit(): bool
    {
        // TODO: Implement commit() method.
    }

    private function checkDir()
    {
        $dir = dirname($this->location);

        if (!is_dir($dir)) {
            mkdir($dir, 755);
        }
    }
}